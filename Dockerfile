FROM node:20-alpine

WORKDIR /home/node/app

COPY . .

RUN cp packages/server/.env-example packages/server/.env

RUN npm install

EXPOSE 3000

CMD [ "npm", "start" ]